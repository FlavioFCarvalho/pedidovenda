package com.algaworks.pedidovenda.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.algaworks.pedidovenda.model.Usuario;
import com.algaworks.pedidovenda.service.CadastroUsuarioService;
import com.algaworks.pedidovenda.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroUsuarioService cadastroUsuarioService;
	
	private Usuario usuario; 
		
	public CadastroUsuarioBean() {
		limpar();
	}

	public Usuario getUsuario() {
		return usuario;
	}



	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
 
	

	/*
	public void inicializar() {

		if (FacesUtil.isNotPostback()) {
			categoriasRaizes = categorias.raizes();

			if (this.categoriaPai != null) {
				carregarSubcategorias();
			}
		}
	}*/

	

	public void limpar() {
		usuario = new Usuario();
		
	}

	public void salvar() {

		this.usuario = cadastroUsuarioService.salvar(this.usuario);
		limpar();

		FacesUtil.addInfoMessage("Usuario salvo com sucesso!");
	}

	public boolean isEditando() {
		return this.usuario.getId() != null;
	}
}