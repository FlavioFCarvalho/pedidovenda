package com.algaworks.pedidovenda.service;

import java.io.Serializable;

import javax.inject.Inject;


import com.algaworks.pedidovenda.model.Usuario;
import com.algaworks.pedidovenda.repository.Usuarios;
import com.algaworks.pedidovenda.util.jpa.Transactional;

public class CadastroUsuarioService implements Serializable {

	@Inject
	private Usuarios usuarios;

	private static final long serialVersionUID = 1L;

	@Transactional
	public  Usuario salvar(Usuario usuario) {
		
		
		/*Usuario usuarioExistente = usuarios.porId(usuario.getId());
		 usuarios.porId(usuario.getId());
		if (usuarioExistente != null && !usuarioExistente.equals(usuario)) {
			throw new NegocioException(
					" Já existe um usuario com o Código informado!");
		}*/

		return usuarios.guardar(usuario);

	}

}
