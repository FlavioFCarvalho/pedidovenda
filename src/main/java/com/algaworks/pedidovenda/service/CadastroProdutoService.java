package com.algaworks.pedidovenda.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.algaworks.pedidovenda.model.Produto;
import com.algaworks.pedidovenda.repository.Produtos;
import com.algaworks.pedidovenda.util.jpa.Transactional;

//Classe responsável pelas RN da classe de produto
public class CadastroProdutoService implements Serializable {

	@Inject
	private Produtos produtos;
	
	private static final long serialVersionUID = 1L;
	
	@Transactional
	public Produto salvar (Produto produto){
		
		Produto produtoExistente = produtos.porSku(produto.getSku());
		
		if(produtoExistente != null && !produtoExistente.equals(produto)){
			throw new NegocioException(" Já existe um produto com o Sku informado!");
		}
		
		return produtos.guardar(produto);
	}
	
	

}
