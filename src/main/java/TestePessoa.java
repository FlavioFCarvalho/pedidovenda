import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.algaworks.pedidovenda.model.Pessoa;
import com.algaworks.pedidovenda.model.TipoClienteFornecedor;


public class TestePessoa {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("PedidoPU");
		EntityManager manager = factory.createEntityManager();
		
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
	    Pessoa cliente = new Pessoa();
		cliente.setNome("Condor");
		cliente.setTipoclientefornecedor(TipoClienteFornecedor.CLIENTE);
		
		Pessoa fornecedor = new Pessoa();
		fornecedor.setNome("Climaz");
		fornecedor.setTipoclientefornecedor(TipoClienteFornecedor.FORNECEDOR);
	   
		
		
		
		manager.persist(fornecedor);
		manager.persist(cliente);
		
		trx.commit();
	}
	
}